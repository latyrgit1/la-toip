---
sidebar_position: 2
---

## Partie 1: Mise en évidence des codecs vidéo

### 1. Installation d'Asterisk

On suppose que vous avez déjà installé astérisk avec les sources

### 2. Configuration de PJSIP

Pour la configuration de **pjsip**, nous allons d'abord activer le pjsip dans le fichier **modules.conf** comme suit:


	cd /etc/asterisk
	vim modules.conf


Ajouter les configurations suivantes: 


	require = chan_pjsip.so
	noload = chan_sip.so


Ensuite arrêtez le serveur asterisk comme suit:


	asterisk -rvvvvvvvvvvvvvvv
	core stop now


Après vous allez redémarrez le serveur comme suit: 


	asterisk
	asterisk -rvvvvvvvvvvvvvv
	reload


### 3. Création des comptes PJSIP avec les codecs vidéo

D'abord, nous allons activer les paramètres sur le transport à **udp**, comme suit: 


	cd /etc/asterisk
	vim pjsip.conf

![Configuration transport-udp](./images/1.png)


Ensuite, pour créer un compte **pjsip**, nous allons éditer le fichier **pjsip.conf** comme suit:

	
	cd /etc/asterisk
	vim pjsip.conf


Et y mettre le contenu suivant:

	[7000]
	type=endpoint
	context=ec2lt-toip
	disallow=all
	allow=h263p
	allow=h264
	allow=ulaw
	allow=vp8
	transport=transport-udp
	auth=7000
	aors=7000

	[7000]
	type=auth
	auth_type=userpass
	password=passer
	username=7000

	[7000]
	type=aor
	max_contacts=30


![Création de compte pjsip](./images/2.png)



**NB:** Nous avons procédé de la même manière pour créer le compte **7001**



### 4. Configuration du plan de numérotation

Pour la configuration du plan de numérotation, nous allons éditer le fichier **extensions.conf** comme suit: 

	cd /etc/asterisk
	vim extensions.conf


![Plan de numérotation](./images/3.png)


### 5. Test des appels vidéo

Pour tester les appels vidéo, nos deux clients sont connectés avec un softphone **zoiper**



### 6.Conférence téléphonique avec pjsip vidéo et Menu


#### 6.1 Étude Conférence avancée avec Confbridge

La conférence est au cœur de la collaboration et permet des équipes distribuées ou virtuelles. Combinée à la connectivité VoIP pour les télétravailleurs, la conférence permet à une équipe de fonctionner de manière simple et abordable dans une zone géographique diversifiée.

#### 6.2 Pourquoi avons-nous besoin de la conférence ?

Asterisk inclut une application standard appelée ConfBridge. ConfBridge est un composant de pont de conférence compatible haute définition qui facilite la création de services de conférence autonomes ou l'intégration de la conférence dans d'autres solutions, y compris les systèmes IP PBX.

La création d'une salle de conférence est triviale, ne nécessitant que quelques lignes de script Dialplan. ConfBridge comprend une multitude de fonctionnalités administratives (couper le son des participants, ajouter/supprimer des appelants, etc.) et une structure d'événements riche qui permet aux développeurs de créer des interfaces utilisateur entièrement intégrées. ConfBridge prend également en charge la visioconférence de base, bien que cette fonctionnalité soit actuellement considérée comme expérimentale.

Nous allons passer à l’étude des concepts de la conférence pour le mieux la conférence avec confbridge.


#### 6.3 Concepts fondamentaux de la conférence

Confbridge propose quatre concepts internes:

- **Numéro de conférence**
- **Profil de pont**
- **Profil utilisateur**
- **Menu conférence**


**Un numéro de conférence** est une représentation numérique d'une instance du pont. Les appelants joints au même numéro de conférence seront dans le même pont de conférence; ils sont connectés. Les appelants joints à différents numéros de conférence ne se trouvent pas dans le même pont de conférence; ils sont séparés. Les numéros de conférence sont attribués dans le plan de numérotation.

**Un profil de pont** est un ensemble nommé d'options qui contrôlent le comportement d'un pont de conférence particulier. Chaque pont doit avoir son propre profil. Un seul pont ne peut pas avoir plus d'un profil de pont.

**Un profil utilisateur** est un ensemble nommé d'options qui contrôlent l'expérience de l'utilisateur en tant que membre d'un pont particulier. Chaque utilisateur participant à un pont peut avoir son propre profil d'utilisateur individuel.

**Un menu de conférence** est un ensemble nommé d'options qui sont fournies à un utilisateur lorsqu'il présente des touches DTMF alors qu'il est connecté au pont. Chaque utilisateur participant à un pont peut avoir son propre menu de conférence individuel.


#### 6.4 Mise en œuvre

Pour entamer le pratique, les fichiers concernés sont au nombre de trois. D'abord on crée des comptes dans le fichier **pjsip.conf**, ensuite on définit le plan de numérotation dans **extensions.conf** puis mettre en place la conférence dans le fichier **confbridge.conf**.

Le fichier **confbridge.conf** renferme les profils, les menus et l'ensemble des modes utilisés.

Ce fichier contient 4 sections qui sont réservées:

- **général**
- **default\_bridge (pont par défaut)**
- **default\_user (utilisateur par défaut)**
- **menu**

La section **default\_bridge** contient toutes les options appelées lorsque Confbridge est instancié à partir du plan de numérotation sans argument de **profil de pont**.

La section **default\_user** contient toutes les options appelées lorsque Confbridge est instancié à partir du plan de numérotation sans de **profil utilisateur.**    

Chaque section contient une définition de type. La définition de type détermine la fonction de la section. Les trois types sont:

- **Pont**
- **Utilisateur**
- **Menu**

**Pont** est utilisé pour désigner les définitions de section de **profil de pont.**

**Utilisateur** est utilisé pour indiquer les définitions de la section Profil utilisateur.

**Menu** est utilisé pour indiquer les définitions de la section Menu Conférence.


#### 6.5 Création de compte

La création des comptes se fera dans le fichier pjsip.conf, et elle est scindée en quatre blocs :

**Le bloc transport :** est définit une seule fois, c’est le protocole qui sera utilisé sur notre réseau. Ces protocoles peuvent être : udp, tcp, tls, ws, wss.

**Le bloc auth :** permet l’authentification de l’utilisateur.

**Le bloc aor :** permet aux utilisateurs de se connecter.

**Le bloc endpoint :** permet la déclaration des utilisateurs.

Nos numéros de compte sont 8000 à 8003.


![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.001.png)       ![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.002.png)


![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.003.png)	     ![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.004.png)


#### 6.6 Établissement de la conférence

Le fichier concerné dans cette partie est confbridge.conf.

On va commencer par définir la section profile, suivi de la section de l’utilisateur et du menu. Vu qu’on a deux types d’utilisateur : les utilisateurs simples et les super utilisateurs à savoir les administrateurs. Chacun aura son propre menu.


![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.005.png)


![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.006.png)     ![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.007.png)


#### 6.7 Étude détaillée des options de chaque section :

**Bloc profile :**

- **profile\_pon :** c’est le nom du bloc de la section profile pour les utilisateurs.

- **Type = bridge :** Permet de définir le type de pont du profil de pont.

- **max\_members :** Permet de définir le nombre maximum de participant dans la conférence. Car par défaut, dans les  conférences n’ont pas de limite du taux de participants. Avec l’option **max-member** une fois la limite d »finit est atteint la conférence est verrouillée.  

- **Mixing-interval :** Équivaut à l’intervalle de mélange ou de mixage. Cette option permet de définir l’intervalle de mixage interne en millisecondes. Par défaut cet intervalle est de 20ms. Pour notre cas on a choisi un intervalle de mixage de 10ms. Entre autre ce paramètre définit à quel point le mixage est serré ou lâché pour la conférence. Si l’intervalle choisie est inférieur dans ce cas le son est plus serré et rencontre un moins de retard dans le pont. Si l’intervalle est élevé dans ce cas le son est lâché et présente des retards dans le pont.

- **Internal-sample-rate :** Ce paramètre renferme plusieurs valeurs. Il permet de définir la fréquence d’échantillonnage choisit. Pour  notre cas on souhaite avoir une fréquence d’échantillonnage automatique, la valeur à choisir est donc auto.

- **record\_conference :** Ce paramètre permet de à l’utilisateur s’il veut enregistrer ou non la conférence. La valeur yes permet d’enregistrée la conférence.

- **video\_mode :** Ce type de paramètre est très utile dans une conférence. Dans les grandes conférences on aura besoin de voir tous les participants de cette salle voir ou plus tot de partager leurs écrans pour diffuser leur travail qui ont été fait. Ainsi ce paramètre ne sera très utile. Il contient plusieurs valeurs :

**Aucun :** aucune source vidéo n’est définie par défaut.

**follow\_talker :** la distribution de la vidéo suit quiconque qui parle dans la salle.

**last\_marked :** le dernier utilisateur rejoignant la salle sera la source vidéo unique distribuée à tous les autres participants. Si ce dernier utilisateur quitte 	la salle, l’utilisateur marqué avant que ce dernier ne rejoint la salle sera à 	nouveau utilisé comme vidéo source ainsi de suite.

**first\_marked :** le premier utilisateur qui à rejoint la salle de conférence sera la source vidéo unique distribuée à tous les participants.

Attention les codecs vidéo utilisés par les participants doivent être le même.

**Bloc utilisateur :**

- **userSimple :** c’est le nom du bloc de la section définit  pour le type d’utilisateurs simples.

- **adminUser :** c’est le nom du bloc de la section définit  pour les administrateurs.


- **type :** Dans ce cas notre type est user. Ce type permet de définir l’utilisateur pour configurer le profil utilisateur.

- **music\_on\_hold\_when\_empty :** Ce paramètre définit si la  musique d’attente doit etre jouée lorsqu’une seule personne participe à la conférence ou lorsqu’un utilisateur attend un utilisateur marqué entre dans la conférence. Elle renferme deux valeurs Oui ou Non. Par défaut c’est désactivé, donc pour jouer la musique si la salle est vide on définit la valeur yes.

- **music\_on\_hold\_class :** Ce paramètre  définit la classe de la musique d’attente qu’on veut utiliser pour notre conférence. Si on ne spécifie notre propre musique la valeur à mettre est **default.** Pour définir notre musique il suffit de mettre le contexte du bloc music qui a été définit dans le fichier **music\_on\_hold.conf**.

- **announce\_user\_count:** Ce paramètre renferme deux valeurs Oui ou Non. S’il est définit à Oui dans ce cas  les différents utilisateurs connectés seront annoncer dès leur entrée dans la salle.

- **announce\_user\_count\_all :** Ce paramètre  est pratiquement le même que celui qui lui est précédé sauf que ici la valeur ajoutée est un nombre entier. Donc il peut contenir soit Oui, Non ou un entier. Si la valeur choisit est OUI, on annonce le nombre d’utilisateurs connectés  à tous les utilisateurs de la conférence. Si on définit un entier, l’annonce va etre produit que lorsqu’on dépasse la limite des participants.

- **dsp\_drop\_silence :** Il supprime ce que le logiciel détecte comme silence entrant dans le pont. Le paramètre renferme deux valeurs OUI ou NON. Le choix de la valeur OUI permet d’améliorer les performances et aidera à éliminer l’accumulation de bruit de fond de la conférence.

- **announce\_join\_leave:** Ce paramètre a été mis en place pour la demande ou non d’un nom d’un utilisateur lorsqu’il entre dans la salle de conférence. Les valeurs prises sont OUI ou NON. Si oui il demandera le nom du user et le nom sera enregistré et lu lorsque l’utilisateur entre ou sort de la conférence. 

- **denoise** : Ce paramètre réduit le bruit appliqué à l’audio avant le mixage. Il peut être OUI ou NON.

- **pin :** Si on veut sécuriser la salle de conférence, on peut faire appel au paramètre pin qui nécessite d’entrer le code définit avant d’avoir accès à la conférence.

**Bloc menu :**

- **[menu\_user]:** C’est le nom du bloc de la section du menu utilisateur.

Cette section renferme plusieurs options qu’on à savoir l’augmentation de la voix, l’augmentation du volume entrant, la diminution de la voix, la diminution du volume entrante, rendre muet ou nom un utilisateur, quitter la conférence etc. Nous allons pas à pas donner et définir tous les options de cette partie.

- **type :** La valeur menue est choisi pour régler ou configurer un menu dans une conférence.

- **toggle\_mute :** Active ou désactive la sourdine d’un user. Lorsqu’un utilisateur est mis sourdine, il ne pourra pas parler aux autres utilisateurs de la conférence, mais il pourra écouter les autres utilisateurs.

- **no\_op :** Cette action n’a pas grande importance dans la section menue. Son objectif est juste de réserver une séquence de configuration comme séquence de sortie de menu.

- **decrease\_listening\_volume :** Diminue le volume d’écoute de l’appelant.

- **increase\_listening\_volume :** augmente le volume d’écoute de l’appelant.

- **reset\_listening\_volume :** Permet de réinitialiser le volume d’écoute de l’appelant au niveau.

- **decrease\_talking\_volume :** Diminue le volume de la conversation de l’appelant.

- **increase\_talking\_volume :** Augmente le volume de conversation de l’appelant.

- **reset\_talking\_volume :** Réinitialise le volume de conversation de l’appelant au niveau.

- **dialplan\_exec :** Cette option permet de sortir de la conférence et d’exécuter des commandes dans le plan de numérotation. Elle renferme les valeurs contexte, étendue et la priorité qui sont les valeurs essentielles définies dans un plan de numérotation.

- **leave\_conference :** Cette fonction permet à un utilisateur de quitter la conférence.

- **admin\_kick\_last :** Ce paramètre donne à un administrateur la possibilité de retirer le dernier participant de la conférence. Ces options sont réservées uniquement que pour les administrateurs.

- **admin\_toggle\_conference\_lock :** Donne à un administrateur de basculer entre le verrouillage et le déverrouillage de la conférence. Attention lorsqu’une conférence est verrouillée seuls les administrateurs pourront le rejoindre.

- **admin\_toggle\_mute\_participants:** Cette option est nouvelle dans Asterisk, il donne le privilège à un administrateur d’activer ou de désactiver le son de tous les participants qui ne sont pas administrateurs de la conférence.

**Attention :** Le fait de dupliquer deux fois le numéro de l’option et de précédé le premier avec une \* (étoile) nous permet simplement de taper le numéro de l’option et ceci est exécuté.

#### 6.8 Définition du plan de numérotation

![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.008.png)

Dans le plan de numérotation, on définit tout d’abord nos différents numéros créés dans notre fameux fichier nommé pjsip.conf.

Puis on définit la conférence dans le plan de numérotation.

L’administrateur pour accéder à la conférence, il compose le numéro 222 suivis de son mot de passe pour accéder la session.

Contrairement aux utilisateurs eux leur numéro est 111 suivi du mot de passe qui va leur permettre d’accéder à la conférence.


#### 6.9 Phase Test

**Au niveau du client**


![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.009.jpeg)       ![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.010.png)


**Au niveau du serveur**

![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.011.png)		![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.012.png)


**Au niveau du client pour la vidéo**

![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.013.png)           ![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.014.jpeg)


**Au niveau du serveur**

![](./images/Aspose.Words.3d4af03d-ddf4-45c3-8b0f-4bb75eb5e837.015.png)


#### 6.10 Conclusion

L'informatique développe de jour en jour. L'implémentation et le développement de nouvelles fonctionnalités augmentent de plus en plus ce qui nous a poussés à la découverte des fonctionnalités administratives de la conférence asterisk avec Confbridge.



## Partie 2: Simplification des configurations avec **pjsip_wizard** 

Pour cette partie allons créer d'autres comptes avec **pjsip_wizard** pour remplacer les comptes déjà créés dans **pjsip.conf**.

Nous allons commenter les comptes dans le fichier **pjsip.conf** et faire les configs dans **pjsip_wizard.conf** comme suit: 

### 1. Configurations

On se déplace dans le dossier astérisk 

	cd /etc/asterisk
	vim pjsip_wizard.conf


Et y ajouter :


	[7000]
	type = wizard
	accepts_auth = yes
	accepts_registrations = yes
	has_phoneprov = yes
	transport = transport-udp
	has_hint = yes
	hint_exten = 7000
	inbound_auth/username = 7000
	inbound_auth/password = passer
	endpoint/allow = ulaw
	endpoint/allow = vp8
	endpoint/allow = h264
	endpoint/context = ec2lt-toip
	aor/max_contacts = 30


![Configuration de compte avec pjsip_wizard](./images/4.png)



**NB:** Nous avons procédé de la même manière pour créer le compte **7001**


Il faut maintenant arrêter le sercveur asterisk comme suit:


	asterisk -rvvvvvvvvvvvvvvvvvvv
	core stop now


Ensuite redémarrer le serveur comme suit:

	asterisk
	asterisk -rvvvvvvvvvv
	reload


### 2. Test le fonctionnement 

Nous allons connecter un utilisateur avec le softphone zoiper pour tester l'appel.  




## Partie 3: Interco entre deux serveurs asterisk avec pjsip

### 1. Installation d'une machine ubuntu 18.04 pour asterisk 2

#### Pré-requis

	sudo apt update
	sudo apt -y upgrade
	sudo apt -y install git curl wget libnewt-dev libssl-dev libncurses5-dev subversion  libsqlite3-dev build-essential libjansson-dev libxml2-dev  uuid-dev
	

#### Installation d'astérisk

	cd /usr/src/
	sudo curl -O http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-16-current.tar.gz
	sudo tar xvf asterisk-16-current.tar.gz
	cd asterisk-16*
	sudo contrib/scripts/get_mp3_source.sh
	sudo contrib/scripts/install_prereq install
	sudo ./configure

![Configuration](./images/5.png)

	sudo make menuselect

![Interface Add-ons](./images/6.png)
![Core Sound Packages](./images/7.png)
![Music On Hold](./images/8.png)

	sudo make

![Make](./images/9.png)

	sudo make install

![Make install](./images/10.png)

	sudo make samples
	sudo make config
	sudo ldconfig
	
### 2. Interconnexion 

#### Cas 1: Interco entre deux serveurs astérisk avec pjsip classique

***#Configuration côté serveur astérisk 1, adresse IP: 192.168.2.144***


-	**Création du compte utilisateur dans le fichier pjsip.conf**


	;Compte utilisateur 7000	
	[7000]

	type=endpoint

	context=ec2lt-toip

	disallow=all

	allow=h263p

	allow=h264

	allow=ulaw

	allow=vp8

	transport=transport-udp

	auth=7000

	aors=7000


	[7000]

	type=auth

	auth_type=userpass

	password=passer

	username=7000


	[7000]

	type=aor

	max_contacts=30



**NB:** Même procédure pour créer le compte 7001 et le compte 7002 utilisé pour le trunk sur l'astérisk 2


-	**Création du compte trunk** 


	;Compte trunk pour astérisk 1

	[6002]

	type=registration

	transport=transport-udp

	outbound_auth=6002

	server_uri=sip:192.168.2.163    ; adresse IP du serveur asterisk 2

	client_uri=sip:6002@192.168.2.163       ; adresse IP du serveur asterisk 2

	retry_interval=60

	expiration=120

	contact_user=6002


	[6002]

	type=endpoint

	context=toip-ec2lt

	disallow=all

	allow=h263p

	allow=h264

	allow=ulaw

	allow=vp8

	transport=transport-udp

	outbound_auth=6002

	aors=6002

	from_user=6002

	from_domain=192.168.2.163 ; adresse IP du serveur asterisk 2


	[6002]

	type=auth

	auth_type=userpass

	password=passer

	username=6002


	[6002]

	type=aor

	contact=sip:192.168.2.163  ; adresse IP du serveur asterisk 2


**NB:** Le compte 6002 est un compte utilisateur crée dans le astérisk 2


-	**Plan de numérotation asterisk 1**


On édite le fichier **/etc/asterisk/extensions.conf** et on ajoute la ligne suivante dans le contexte des utilisateurs:


	[ec2lt-toip]
	exten => _6XXX,1,Dial(PJSIP/${EXTEN}@6002)  ;Tout appel commençant avec 6 suivi de trois chiffres sera envoyé vers le trunk 


***#Configuration côté serveur astérisk 2, adresse IP: 192.168.2.163***



-	**Création du compte utilisateur dans le fichier pjsip.conf**


        ;Compte utilisateur 6000

        [6000]
        type=endpoint
        context=toip-ec2lt
        disallow=all
        allow=h263p
        allow=h264
        allow=ulaw
        allow=vp8
        transport=transport-udp
        auth=6000
        aors=6000

        [6000]
        type=auth
        auth_type=userpass
        password=passer
        username=6000

        [6000]
        type=aor
        max_contacts=30


**NB:** Même procédure pour créer le compte 6001 et le compte 6002 utilisé pour le trunk sur l'astérisk 1


-	**Création du compte trunk**


        ;Compte trunk pour astérisk 1

        [7002]
        type=registration
        transport=transport-udp
        outbound_auth=7002
        server_uri=sip:192.168.2.144    ; adresse IP du serveur asterisk 1
        client_uri=sip:6002@192.168.2.144       ; adresse IP du serveur asterisk 1
        retry_interval=60
        expiration=120
        contact_user=7002

        [7002]
        type=endpoint
        context=ec2lt-toip
        disallow=all
        allow=h263p
        allow=h264
        allow=ulaw
        allow=vp8
        transport=transport-udp
        outbound_auth=7002
        aors=7002
        from_user=7002
        from_domain=192.168.2.144 ; adresse IP du serveur asterisk 1

        [7002]
        type=auth
        auth_type=userpass
        password=passer
        username=7002

        [7002]
        type=aor
        contact=sip:192.168.2.144  ; adresse IP du serveur asterisk 1



**NB:** Le compte 7002 est un compte utilisateur crée dans le astérisk 1


-	**Plan de numérotation asterisk 2**

On édite le fichier **/etc/asterisk/extensions.conf** et on ajoute la ligne suivante dans le contexte des utilisateurs:


        [toip-ec2lt]
        exten => _7XXX,1,Dial(PJSIP/${EXTEN}@7002)  ;Tout appel commençant avec 7 suivi de trois chiffres sera envoyé vers le trunk


***#Test de fonctionnement avec les comptes pjsip classiques***

Pour tester, nous allons connecter un utilisateur 7000 sur le serveur asterisk 1 et un utilisateur 6000 sur le serveur asterisk 2. 


#### Cas 1: 7000 appel 6000 


Côté serveur asterisk 1

![Appel de 7000 vers 6000 (serveur 1)](./images/13-1.png)


![Appel de 7000 vers 6000 (serveur 1)](./images/14-1.png)



Côté serveur asterisk 2

![Appel de 7000 vers 6000 (serveur 2)](./images/13-2.png)


![Appel de 7000 vers 6000 (serveur 2)](./images/14-2.png)



#### Cas 2: 6000 appel 7000 


Côté serveur astérisk 1


![Appel de 6000 vers 7000 (serveur 1)](./images/11-1.png)


![Appel de 6000 vers 7000 (serveur 1)](./images/12-1.png)



Côté serveur astérisk 2


![Appel de 6000 vers 7000 (serveur 2)](./images/11-2.png)


![Appel de 6000 vers 7000 (serveur 2)](./images/12-2.png)



#### Capture de l'appel vidéo

Dans tous les deux cas, l'appel vidéo est passé.


**Téléphone 7000**

![Appel vidéo 7000 ](./images/15.jpeg)



**Téléphone 6000**

![Appel vidéo 6000](./images/15-1.jpeg)



#### Cas 2: Interco entre deux serveurs astérisk avec pjsip_wizard

Pour faire cette interconnexion, nous allons mettre en commentaire les configurations faites dans les fichiers **pjsip.conf** de part et d'autres des deux serveurs astérisk.

Nous allons maintenant configurer les fichiers **pjsip_wizard.conf** dans les deux côtés des serveurs astérisk et définir les trunks.

**NB:** Le principe de la crétaion des comptes d'utilisateurs reste le même que cette dans l'étape 2 du support.

Nous allons alors définir directement les comptes trunk dans les deux serveurs.

-	**Création du trunk avec pjsip_wizard dans astérisk 1**


	;Compte trunk vers asterisk 2

	[6002]
	type = wizard 
	sends_auth = yes
	sends_registrations = yes
	accepts_registrations = yes
	accepts_auth = yes
	remote_hosts = 192.168.2.163:5060   ;l'IP du serveur asterisk 2
	outbound_auth/username = 6002
	outbound_auth/password = passer
	endpoint/context = ec2lt-toip
	endpoint/allow = ulaw
	aor/qualify_frequency = 60
	aor/max_contacts = 30


**NB:** Le compte **6002** est un compte utilisateur pjsip_wizard crée dans le serveur astérisk 2


-	**Création du trunk avec pjsip_wizard dans astérisk 2**


	;Compte trunk vers asterisk 1

	[7002]
	type = wizard 
	sends_auth = yes
	sends_registrations = yes
	accepts_registrations = yes
	accepts_auth = yes
	remote_hosts = 192.168.2.144:5060   ;l'IP du serveur asterisk 2
	outbound_auth/username = 7002
	outbound_auth/password = passer
	endpoint/context = toip-ec2lt
	endpoint/allow = ulaw
	aor/qualify_frequency = 60
	aor/max_contacts = 30


**NB:** Le compte **7002** est un compte utilisateur pjsip_wizard crée dans le serveur astérisk 1 


Il faut maintenant arrêter les serveurs astérisk et les redémarrer, comme suit:


	asterisk -rvvvvvvvvvvvvvvvvv
	core stop now


Ensuite faire:


	asterisk
	asterisk -rvvvvvvvvvvvvvvvvvv
	reload



***#Test de fonctionnement avec les comptes pjsip_wizard***


#### Cas 1: 7000 appel 6000 


Côté serveur asterisk 1


![Appel de 7000 vers 6000 (serveur 1)](./images/17-1.png)


Côté serveur asterisk 2


![Appel de 7000 vers 6000 (serveur 2)](./images/17-2.png)



#### Cas 2: 6000 appel 7000 


Côté serveur asterisk 1


![Appel de 6000 vers 7000 (serveur 1)](./images/16-2.png)


Côté serveur asterisk 2


![Appel de 6000 vers 7000 (serveur 2)](./images/16-1.png)



#### Capture de l'appel vidéo avec pjsip_wizard


Dans tous les deux cas, l'appel vidéo est passé.

**Téléphone serveur 1 numéro 7000**


![Appel vidéo 7000 vers 6000 ](./images/18.jpeg)


**Téléphone serveur 2 numéro 6000**


![Appel vidéo 6000 vers 7000 ](./images/19.jpeg)



## Partie 4: Astérisk et webRTC (sipml5, cyber_mega_phone_2k et ctxphone)


### 1. Pré-requis

	sudo mkdir /etc/asterisk/keys
	sudo contrib/scripts/ast_tls_cert -C pbx.ec2lt.sn -O "EC2LT" -b 2048 -d /etc/asterisk/keys


![Génération de clé](./images/20.png)


	sudo ls /etc/asterisk/keys/


### 2. Configuration


Nous allons d'abord éditer le fichier **/etc/asterisk/http.conf** pour mettre les configs comme suit:

	
	[general]
	enabled=yes
	bindaddr=0.0.0.0
	bindport=8088
	tlsenable=yes
	tlsbindaddr=0.0.0.0:8089
	tlscertfile=/etc/asterisk/keys/asterisk.crt 
	tlsprivatekey=/etc/asterisk/keys/asterisk.key 
	tlscafile=/etc/asterisk/keys/ca.crt


![Configuration dans http.conf](./images/22.png)


Vous pouvez vérifier si tout marche en faisant:


![Vérification des configs](./images/21.png)


### 3. Configuration PJSIP


Pour la configuration, nous allons activer le transport **wss** comme suit dans le fichier **pjsip.conf** comme suit:


	[transport-wss]
	type=transport
	protocol=wss
	bind=0.0.0.0


Ensuite définir les comptes utilisateurs dans le fichier **pjsip.conf** comme suit:


	[200]
	type=endpoint
	aors=200
	auth=200
	direct_media=no
	allow=!all,ulaw,vp8,h264
	max_audio_streams=10
	max_video_streams=10
	webrtc=yes
	dtls_cert_file=/etc/asterisk/keys/asterisk.pem
	dtls_ca_file=/etc/asterisk/keys/ca.crt
	context=ec2lt-toip
	
	[200]
	type=auth
	auth_type=userpass
	username=200
	password=passer

	[200]
	type=aor
	max_contacts=30
	remove_existing=yes


**NB:** Un autre compte **201** et **202** ont été créé de la même manière 


### 4. Téléchargement les applications Sipml5 et cyber_mega_phone_2k

Pour télécharger sipml5, vous allez faire:


	cd /var/lib/asterisk/static-http/
	apt-get install git	
	git clone https://github.com/DoubangoTelecom/sipml5.git
	git clone https://github.com/asterisk/cyber_mega_phone_2k.git


Ensuite il faut gérer la reedirection dans le fichier **http.conf** comme suit: 


	cd /etc/asterisk
	vim http.conf

Et adaptez la ligne **redirect** comme suit: 


	redirect = /sipml5 /static/sipml5/index.html
	redirect = /phone2k /static/cyber_mega_phone_2k/index.html


Ce qui veut dire, vous pourrez accéder directement à votre application en faisant: *https://votre_adresse_ip:8089/sipml5* ou *https://votre_adresse_ip:8089/phone2k*  et vous serez rediriger vers votre application.

**NB:** Donc on note que Astérisk à lui seul peut servir de serveur web.


Il faut maintenant récharger votre serveur astérisk avec:


	asterisk -rvvvvvvvvvvvvvvvv
	reload


Vous pouvez vérifier si le port 8089 est bien ouvert en faisant:


	netstat -anp | grep 8089


Et vous obtenez:


![port 8089](./images/23.png)


### Configuration Confbridge

Pour configurer confbridge, nous allons éditer le fichier **confbridge.conf** comme suit:

	[default_user]
	type=user
	marked=yes
	music_on_hold_when_empty=yes
	music_on_hold_class=default
	announce_user_count=yes
	wait_marked=yes
	end_marked=yes

	
	[default_bridge]
	type=bridge
	video_mode=sfu
	max_members=50   


### Configuration de extensions.conf


Pour configurer confbridge, nous allons éditer le fichier **extensions.conf** comme suit:


	[ec2lt-toip]
	exten => _7XXX,1,Dial(PJSIP/${EXTEN})

	exten => _6XXX,1,Dial(PJSIP/${EXTEN}@6002)  ;Tout appel commençant avec 6 sera envoyé vers le trunk 

	exten => 3000,1,Progress()
	exten => 3000,2,wait(1)
	exten => 3000,3,Confbridge(1,default_bridge,default_user)



### 5. Test de fonctionnement

#### 5.1 Utilisation de cyber_mega_phone_2k

Nous pouvons alors accéder à l'application en faisant: *https://votre_adresse_ip:8089/phone2k* comme suit:


![Application cyber_mega_phone_2k](./images/30.png)


On clique sur le button **Account** pour enrégistrer un utilisateur comme suit:


![Configuration du compte 200](./images/31.png)


Il faut maintenant cliquer sur le button **Connect** pour  connecter l'utilisateur comme suit:


![Utilisateur connecter](./images/32.png)


Sur la console d'astérisk, on peut observer :


![Console asterisk après connxion de 200](./images/33.png)


**NB:** Les mêmes configurations on été faites pour connecter les utilisateurs **201** et **202**


On peut maintenant tester l'appel.


![Conférence entre les utilisateurs 200, 201 et 202 ](./images/34.png)



#### 5.2 Utilisation de Sipml5

Nous pouvons alors accéder à l'application en faisant: *https://votre_adresse_ip:8089/sipml5* comme suit:


![Application sipml5](./images/24.png) 


Ensuite on clique sur le bouton **Enjoy our live demo** et nous serons rediriger vers cette interface:


![Interface sipml5](./images/25.png)


On clique sur **Expert mode?** pour configurer la partie avancée comme suit:


![Mode expert](./images/26.png)


Configurons maintenant le compte 8000 dans sipml5.

Dans l'interface **Expert mode** et on clique sur **save**


![config Mode expert](./images/27.png)


Dans l'interface **client**  et on clique sur **login**


![config client](./images/28.png)




## Partie 5: Sécurité Asterisk avec pjsip (transport tls, activation srtp)


### 1. Sécurisation de communication sous asterisk

On rappelle que TLS fournit un chiffrement pour la signalisation des appels pour éviter que des personnes mal intentionnées sachent qui les utilisateurs d’asterisk appellent.
La configuration de TLS entre asterisk et un client SIP implique :
-	la création de fichiers clés, la modification de la configuration SIP d'Asterisk pour activer TLS, ;
-	la création d'un compte SIP capable d’utiliser TLS
-	la configuration d’un client SIP pour se connecter à asterisk via TLS
-	L’utilisation de SRTP par asterisk et le client permet de sécuriser le flux media.

Il existe trois modes de cryptage RTP populaires :
-	SRTP MIKEY / SDES (nécessite le cryptage TLS de la signalisation)
-	SRTP DTLS
-	ZRTP


### 2. Mise en œuvre

#### Création des certificats côté serveur

La création des certificats côté serveur a été déjà faite dans l'étape 4.

On peut vérifier dans le dossier **/etc/asterisk/keys** comme suit:

	ls /etc/asterisk/keys


![Certificats serveur](./images/35.png)


#### Création des certificats côté client

On utilise pour cela la commande ci-apres :

	/usr/local/asterisk-16.25.0/contrib/scripts/ast_tls_cert -m client -c /etc/asterisk/keys/ca.crt -k /etc/asterisk/keys/ca.key -C phone1.ec2lt.local -O "EC2LT" -d /etc/asterisk/keys -o 100


![Certificats client](./images/36.png)


ou
-	l’option **m** permet de préciser que nous voulons générer un certificat client
-	l’option **c** permet d’indiquer le certificat de l’autorité de certification
-	l’option **k** permet d’indiquer la clef privée de l’autorité de certification
-	l’option **o** permet de spécifier le nom de la clef privée du client


Dans l »exemple ci-apres, nous avons generé trois certificats pour les utilisateurs **100** et **101** et **102**, et seynabou et on consultant le répertoire /etc/asterisk/keys/ voici ce que nous voyons :

![Liste des certificats des clients](./images/37.png)

On voit :
-	100.csr, 101.csr et 102.csr : qui sont des fichiers demandes de certificat
-	100.key, 101.key et 102.key : qui sont des fichiers clefs privées
-	100.crt, 101.crt et 102.crt : qui sont des certificats clients
-	100.pem, 101.pem et 102.pem : qui sont des concaténations des certificats clients et clefs privées


#### Activation de la sécurisation de la signalisation SIP au niveau d’asterisk

On édite le fichier **/etc/asterisk/pjsip.conf**, on doit avoir les lignes suivantes : 


	[transport-tls]
	type=transport
	protocol=tls
	bind=0.0.0.0:5061
	cert_file=/etc/asterisk/keys/asterisk.crt
	priv_key_file=/etc/asterisk/keys/asterisk.key
	method=sslv23



	[100]
	type=aor
	max_contacts=30
	remove_existing=yes

	[100]
	type=auth
	auth_type=userpass
	username=100
	password=passer

	[100]
	type=endpoint
	aors=100
	auth=100
	transport=transport-tls
	context=ec2lt-toip
	disallow=all
	allow=g722,vp8,h264
	dtmf_mode=rfc4733


**NB:** On a configuré aussi les numéro **101** et **102**

**NB:** Il faut redemarrer carrement asterisk pour que le port TLS par defaut 5061 soit ouvert : 

On remarque qu’asterisk ecoute maintenant aussi sur le port 5061 pour la signalisation.


![Liste des certificats des clients](./images/38.png)


#### Installation de BLINK sur ubuntu 18

-	On installe la clé sécurité de blink pour que votre machine accepte le logiciel blink
	

	apt-get install curl
	curl -o /etc/apt/trusted.gpg.d/agp-debian-key.gpg http://download.ag-projects.com/agp-debian-key.gpg


-	on ajoute les deux lignes suivantes à la fin du fichier **/etc/apt/sources.list**


	deb http://ag-projects.com/ubuntu bionic main
	deb-src http://ag-projects.com/ubuntu bionic main


-	on fait la mise à jour des sources de nos logiciels


	apt-get update


-	on installe blink


	apt-get install blink


#### Copiez les fichiers **100.pem**, **101.pem** et **ca.crt** dans le dossier **/home/user/**.

**Exemple:** Dans mon cas j'ai installé le client blink sur une machine et envoyé les fichiers par SCP dans cette machine:

	scp /etc/asterisk/keys/ca.crt latyr@192.168.1.238:/home/latyr/
	scp /etc/asterisk/keys/100.pem latyr@192.168.1.238:/home/latyr/
	
**NB:** De la même manière on envoie les certificats du compte **101** dans la machine ou le second client blink est installé.
 

-	Pour lancer blink, dans un terminal on tape : **blink &**

-	On parametre blink en precisant la prise en charge du transport TLS et de chiffrement SRTP


![Transport tls SRTP](./images/39.png)


On clique sur **Server Settings**


![server settings](./images/40.png)


Dans server setting , j’ai précisé l’adresse d’asterisk, le port d’écoute de SIPS(5061) ainsi que le transport TLS


On clique sur **Media**


![media](./images/41.png)


Dans media, on voit les codecs audio et video supportés par blink et surtout nous avons activé la securisation des media audio et video en cochant « send inband DTMF »et « encrypt audio and video »

Le dernier parametre mis SDES mandatory permet d’imposer l’utilisation absolue de la securité


On clique maintenat sur **Advanced**


![Advanced](./images/42.png)


Et en fin notre compte **100** est bien connecté:


![compte 100 connecté](./images/43.png)


Sur la console d'asterisk, on voit que **100** est bien connecté:

![compte 100 connecté console asterisk](./images/44.png)


**NB:** l'utilisateur **101** est bien connecté en suivant les mêmes procédures


### Test de focntionnement


![compte 100 connecté console asterisk](./images/45.png)


![compte 100 connecté console asterisk](./images/46.png)


![compte 100 connecté console asterisk](./images/47.png)



## Partie 6: Interco entre asterisk et CME avec pjsip 


### 1. Configuration côté Astérisk

Pour interconnecter asterisk avec un cme, nous allons d'abort définir les comptes comme suit:

Dans **pjsip.conf** 

	;Configuration Asterisk CME

	;Compte utilisateur 300

	[300]
	type=endpoint
	context=ec2lt-toip
	disallow=all
	allow=h263p
	allow=h264
	allow=ulaw
	allow=vp8
	transport=transport-udp
	auth=300
	aors=300

	[300]
	type=auth
	auth_type=userpass
	password=passer
	username=300

	[300]
	type=aor
	max_contacts=30


	;Compte trunk vers CME

	[1001]
	type=registration
	transport=transport-udp
	outbound_auth=1001
	server_uri=sip:192.168.2.10    ;adresse IP du cme
	client_uri=sip:1001@192.168.2.10
	retry_interval=60
	expiration=120
	contact_user=1001
	line=yes
	endpoint=1001
	
	[1001]
	type=endpoint
	context=ec2lt-toip
	disallow=all
	allow=h263p
	allow=h264
	allow=ulaw
	allow=vp8
	outbound_auth=1001
	aors=1001
	from_user=1001
	from_domain=192.168.2.10
	direct_media=no

	[1001]
	type=auth
	auth_type=userpass
	password=passer
	username=1001
	
	[1001]
	type=aor
	contact=sip:192.168.2.10
	contact=sip:192.168.2.10
	qualify_frequency=60
	
	[1001]
	type=identify
	endpoint=1001
	match=192.168.2.10

	;Fin CME


Dans le fichier **extensions.conf** 


	exten => _1XXX,1,Dial(PJSIP/${EXTEN}@1001) ;pour envoyer les appels vers le cme
	exten => _2XXX,1,Dial(PJSIP/3${EXTEN:2}) ;pour dire si un appel arrive avec 2000 par exemple, d'enlever le 20 et remplace par3: exemple appel 2000 => appel 300 


### 2. Configuration côté CME

#### L'architecture mise en oeuvre


![architecture cme](./images/arch.png)


Dans cette architecture, nous avons une images cisco *c3745-ipvoice_ivs-mz.124-15.T7.bin* que vous pouvez télécharger à l'adresse : [cisco ios](https://tfr.org/cisco-ios/37xx/3745/)


#### Configuration

Les configurations sont:

	
	conf t
	interface FastEthernet0/0
	ip address 192.168.2.10 255.255.0.0
	no sh

	
	hostname CME
	

	voice service voip 
	allow-connections sip to sip
	sip      
	 registrar server


	voice register global
	 mode cme 
	 max-dn 30
	 max-pool 30
	 authenticate realm 192.168.2.10



	voice register dn  1
	 number 1000
	 name latyr


	voice register dn  2
	 number 1001
	 name khady


	voice register dn  3
	 number 1002
	 name seynabou


	voice register pool  1
	 id mac 0000.0000.1000
	 number 1 dn 1
	 username 1000 password passer
	 codec g711ulaw


	voice register pool  2
	 id mac 0000.0000.1001
	 number 1 dn 2
	 username 1001 password passer
	 codec g711ulaw


	voice register pool  3
	 id mac 0000.0000.1002
	 number 1 dn 3
	 username 1002 password passer
	 codec g711ulaw


Définition plan de numérotation


	dial-peer voice 10 voip
	 destination-pattern 1...
	 session protocol sipv2
	 session target sip-server
	 codec g711ulaw


Plan de numérotation pour les appels sortants

	
	dial-peer voice 30 voip
	 destination-pattern 2...
	 session protocol sipv2
	 session target sip-server
	 codec g711ulaw


Configuration du trunk 


	sip-ua    
	 authentication username callmanager password 7 071F205F5D0C0B
	 sip-server ipv4:192.168.2.110    ;Adresse ip du serveur asterisk
	


### 3. Test de fonctionnement

Pour tester le fonctionnement, nous allons utiliser un client **zoiper** qui se connecte sur le **cme** et un autre client qui se connecte sur l'asterisk

Si le client se connecte sur le cme, on obtient:


![Console cme](./images/cme.png)


De même s le compte trunk se connecte on obtient:

 
![Compte trunk se connecte sur le cme](./images/cme1.png)



#### Appel de astérisk vers cme


Si le client d'asterisk appel vers le cme, on a:


![Console asterisk](./images/48.png)


On peut voir les clients en communication

-	Client zoiper  connecté sur le asterisk


![Client zoiper sur le asterisk](./images/49.jpeg)


-	Client csipsimple connecté sur le cme


![Client csipsimple sur le cme](./images/50.jpeg)



#### Appel du cme vers astérisk

Nous avons définit dans le cme pour les appels sortants de composer 2 suivi de trois chiffres comme **2...**

Alors l'utilisateur **1000** du cme doit composer **2000** pour appeler l'utilisateur **300** d'astérisk.

Pour cela nous avons définit dans le fichier **extensions.conf** d'astérisk:

	exten => _2XXX,1,DIAL(PJSIP/3${EXTEN:2}) ; pour dire si un appel arrive par exemple avec avec 2000 il est remplacé par 300


**Utilisateur 1000 du cme veut appeler 300 d'astérisk** il compose **2000**


**Utilisateur 1000 du cme**


![Utilmisateur 1000 du cme](./images/cme1.jpeg)


**Utilisateur 300 d'asterisk**


![Utilisateur 300 d'asterisk](./images/ast3.jpeg)



**Console d'astérisk**


![Console asterisk pour appel cme vers asterisk](./images/cme-ast.png)




## Etape 7: Interco asterisk et kamailio avec pjsip

### 1. Présentation de Kamailio

Kamailio est un serveur SIP libre (anciennement OpenSER).

**Fonctionnalités**

Voici les fonctionnalités implémentées dans le logiciel selon l'équipe du projet :

-	Serveur SIP conforme à la RFC3261

-	Serveur d'enregistrement,

-	Serveur de localisation,

-	Serveur proxy,

-	Serveur d'application SIP,

-	Serveur de redirection.

**Configuration**

Le fichier **kamailio.cfg** contient les informations principales de configuration de Kamailio. 
Les sections présentes sont les suivante:

-	**Définitions globales (Global Parameters)** : Cette section du fichier liste les paramètres d'exécution du programme. On y trouve principalement le niveau de débogage, le type de couche de transport utilisé (UDP ou TCP), l'alias du serveur, les adresses IP et les ports d'écoute. Il faut redémarer kamailio pour recharger ces paramètres.

-	**Paramètres locaux (Custom Parameters)** : Ces paramètres peuvent être modifiés en cours d'exécution grâce au module 'cfg_rpc'.

-	**Modules utilisés (Modules Section)** : Cette section du fichier liste les modules chargés au démarrage de Kamailio, ainsi que le chemin pour trouver les modules (mpath). Pour définir des paramètres à ces modules, la commande modparam est utilisée. Les paramètres sont aussi listés dans cette section.

-	**Routage et automate (Routing Logic)** : Cette section définit comment le serveur réagit à un message SIP ou à un événement. C'est l'automate du serveur SIP. L'algorithme livré initialement est censé être conforme aux normes SIP, mais il peut être modifié dans cette section justement. La routine route permet de définir cela.


#### a. Définitions globales (Global Parameters)

-	debug : niveau de log compris entre -3 et 4. Le niveau par défaut est 2 et les logs sont inscrits dans /var/log/syslog. Les niveaux sont les suivants : -3 (alerte) : erreurs nécessitant une action immédiate ; -2 (critique) : erreur causant une situation critique ; -1 (erreur) : erreur pendant le traitement des données mais n'engendrant pas de dysfonctionnement ; 1 (warning) : warning ; 2 (notice) : situation non usuelle ; 3 (informatif) : messages d'informations ; 4 (débogage) : pour le débogage,

-	fork : paramètre indiquant si le processus tournera lors de son lancement en tâche d'arrière-plan,
log_stderror : paramètre pour rediriger les messages d'erreur vers la sortie standard,

-	children : définit le nombre d'enfants (sous-processus) à créer par interface pour traiter les requêtes entrantes, il est utilisé uniquement pour les interfaces UDP et n'a pas d'impact sur les processus TCP.

-	tcp_max_connections :

-	alias :

-	listen :


#### b. Paramètres locaux (Custom Parameters)

-	pstn.gw_ip

#### c. Modules utilisés (Modules Section)

-	mpath : Le chemin pour trouver les bibliothèques des modules à charger (*.so en général). Voici la syntaxe pour ce paramètre : mpath="/tmp/blabla/:/home/ect/:/home/usw/:/home/aso/",

-	loadmodule : La commande loadmodule permet de charger les modules. La syntaxe est la suivante : loadmodule "module.so"

-	modparam : La commande modparam permet de configurer les modules chargés. La syntaxe est la suivante : modparam("nom_du_module","nom_du_paramètre_du_module","valeur_du_paramètre_du_module");

Chaque module possède un fichier de documentation appelé README, souvent sous le répertoire /usr/share/doc/kamailio/modules*/.

#### d. Routage et automate (Routing Logic)

Chaque requête SIP reçue sera traitée dans la boucle commençant par :

route {

elle se termine avec :

}

Lorsqu'un mot clef MotClef est utilisé en paramètre, la fonction

route[MotClef] {

...

}

est exécutée 


### 2. Installation de kamailio

#### Installation de MYSQL:

	apt-get install mysql-server libmysqlclient-dev


#### Installation des prerequis Kamailio:

	apt-get install flex bison libssl-dev libcurl4-openssl-dev libpcre3-dev


#### Installation de Kamailio :

	apt-get install kamailio kamailio-utils-modules kamailio-mysql-modules kamailio-carrierroute-modules kamailio-presence-modules kamailio-xmpp-modules kamailio-python-modules kamailio-xml-modules kamailio-websocket-modules kamailio-tls-modules kamailio-presence-modules


### 3. Configuration de Kamailio

Éditer le fichier **/etc/default/kamailio** pour activer:

	RUN_KAMAILIO = yes


#### Configuration de la base de données d'enregistrement des utilisateurs

On édite le fichier **/etc/kamailio/kamctlrc** et on renseigne le domaine, le nom d'utilisateur et le mot de passe de connexion à la base de données mysql afin de permetre à kamailio de stocker les informations de présence des utilisateurs.


![Configuration kamctlrc](./images/kam1.png)


On installe la base de données kamailio grace à la commande :

	kamdbctl create

![Création de la base de données kamailio](./images/kam2.png)


On voit bein que la base données kamailio est créée en faisant :

	mysql -u root -p

	> show databases ;


![Base de données kamailio](./images/kam3.png)


Ensuite nous allons créer des utilisateurs kamailio pour éffectuer des appels.

La création des utilisateurs se fait par la commande :

	kamctl add XXX password

![Création des utilisateurs](./images/kam4.png)


Avec **XXX** l'utilisateur que nous voulons créer et password son mot de passe de connexion.
Ici nous avons créer les utilisateurs **500, 501, 503**.

Ensuite pour vérifier les utilisateurs crées on fait :

	mysql -u root -p

	>use kamailio ;
	
	>select * from subscriber ;

![Liste des utlisateurs de la base de données kamailio](./images/kam5.png)


### 4. Test fonctionnement

-	**Communication entre 500 et 501** 

**500**

![Utlisateur 500](./images/kam6.jpeg) 

**501**
 
![Utlisateur 501](./images/kam7.jpeg)


### 5. Interconnection entre Kamailio et asterisk

#### 5.1 Configuration côté Kamailio

On édite de fichier de configuration de kamailio **/etc/kamailio/kamalio.cfg** et on ajoute la route d'astérisk dans la section **request_route** comme suit:

	#route ASTERISK
        route(ASTERISK);


Ensuite on définit la route toujours dans le même fichier dans la section où les routes sont définis comme suit: 

	route[ASTERISK] {

        	if(($rU=~"^7[0-9][0-9][0-9]$")) {

                	$ru = "sip:" + $rU + "@" + "192.168.2.110:5060";  #Adresse ip d'astérisk
                	route(RELAY);
                	exit;
        	}
	}


Enfin on redémarre le serveur kamailio avec la commande suivante:

	service kamailio restart


#### 5.2 Configuration côté Astérisk

On suppose que vous avez déjà des comptes pjsip.

Dans le fichier de configuration **pjsip.conf** on définit un trunk comme suit:


	;Compte trunk pour astérisk 1

	[502]
	type=registration
	transport=transport-udp
	outbound_auth=502
	server_uri=sip:192.168.2.123    ; adresse IP du serveur kamailio
	client_uri=sip:502@192.168.2.123
	retry_interval=60
	expiration=120
	contact_user=502
	line=yes
	endpoint=502

	[502]
	type=endpoint
	context=ec2lt-toip
	disallow=all
	allow=h264
	allow=ulaw
	allow=vp8
	direct_media=no
	transport=transport-udp
	outbound_auth=502
	aors=502
	from_user=502
	from_domain=192.168.2.123
	
	[502]
	type=auth
	auth_type=userpass
	password=passer
	username=502
	
	[502]
	type=aor
	contact=sip:192.168.2.123
	qualify_frequency=60
	
	[502]
	type=identify
	endpoint=502
	match=192.168.2.123
	
	;Fin TRUNK 


Ensuite dans le fichier **extensions.conf** dans votre contexte, on définit le plan de numérotation comme suit:

	exten => _5XX,1,Dial(PJSIP/${EXTEN}@502)

#### 5.3 Test de fonctionnement

##### Appel d'Astérisk vers Kamailio

**Utlisateur 7000 d'Asterisk**

![Utlisateur 7000 d'Asterisk](./images/ast1.jpeg)


**Utlisateur 500 de Kamailio**

![Utlisateur 500 de Kamailio](./images/kam8.jpeg)


**Côté serveur Astérisk**


![Console asterisk appel vers kamailio](./images/ast-kam.png)


##### Appel de Kamailio vers Astérisk

**Utlisateur 500 de Kamailio**

![Utlisateur 500 de Kamailio](./images/kam9.jpeg)


**Utlisateur 7000 d'Asterisk**

![Utlisateur 7000 d'Asterisk](./images/ast2.jpeg)


**Côté serveur Astérisk**

![Console asterisk appel vers kamailio](./images/kam-ast.png)




## Etape 8: Interconnection entre Astérisk  et Freeswitch avec pjsip


### 1. Présentation de Freeswitch

-	**Historique de Freeswitch**

Freeswitch est une plateforme de téléphonie open source conçue pour router ou interconnecter les protocoles de communications populaires en utilisant l'audio, la vidéo, le texte ou toute autre forme de média. Il a été créé en 2006, il fournit également une plateforme de téléphonie stable sur laquelle de nombreuses applications peuvent être développées en utilisant une large gamme d'outils gratuits.

-	**Philosophie de Freeswitch**

Freeswitch est une solution open source de téléphonie sur IP, sous une licence MPL (Mozilla Public License), développé en C. Il peut être utilisé comme un simple commutateur, un IPBX, une passerelle ou un serveur d'applications IVR (Interactive Voice Response) en utilisant des scripts ou des fichiers XML permettant d'automatiser certaines taches et de développer de nouveaux services.Freeswitch fonctionne sur plusieurs systèmes d'exploitation, notamment Windows, Mac OS X, Linux, BSD et sur les deux plates-formes Solaris (32 bits et 64 bits).

Freeswitch supporte les caractéristiques standards et avancées du protocole SIP, permettant de mettre en place un serveur de conférence, un serveur de Voicemail ,...

Il utilise aussi les protocoles IAX2, Jingle et H323, Les langages de programmation supportés par cette solution sont :

-	JavaScript

-	Python

-	Perl

-	Lua

-	C/C++

-	JAVA


-	**Les notions de modules**

Freeswitch est modulaire et comprend des modules qui offrent de nombreuses applications : conférence, réponse vocale interactive, synthèse et reconnaissance vocale, messagerie instantanée . La fonctionnalité de Freeswitch peut être étendue par l'ajout de modules qui exécutent une tâche particulière, si cette tâche est simple ou complexe. Les modules peuvent être regroupés en grandes catégories comme marqués avec des étiquettes sur leurs pages de modules individuels.

![Modules freeswitch](./images/free1.png)


-	**Présentation de quels modules**

*Module sms*

Le module sms permet d'effectuer du chat personnalisé entre les clients du serveur.Il fournit un moyen pour acheminer les messages dans FreeSwitch, permettant potentiellement l' un pour construire un système de discussion puissant comme dans XMPP en utilisant SIP simple sur les clients SIP.

*Module smpp*

Le module smpp permet de joindre des clients des autres opérateurs. SMPP est le sigle de Short Message Peer to Peer, un protocole standard d'échange qui permet d'envoyer des SMS vers des opérateurs téléphoniques.


-	**La notion de contexte**

Freeswitch utilise plusieurs «contextes» pour empêcher les extensions internes d'être exposés au monde. Les deux contextes dans la configuration de vanille FS sont appelés "Public" et "Default" (mais ces noms sont arbitraires et peuvent être soigneusement modifiés ou ajoutés d' autres contextes).

**Default** : c'est le contexte par défaut pour tous les utilisateurs inscrits

**Public** : c'est le contexte utilisé pour les appels provenant de l'extérieur


-	**La notion de sip_profiles**

Les profils par défaut sont «interne» et «externe» et servent chacun un objectif général.profils SIP externes sont généralement utilisés pour communiquer avec votre fournisseur de services «Gateway», comme société similaire fournissant un service de téléphonie via le protocole SIP pour vous. Le sip_profile interne est généralement utilisé pour communiquer avec les périphériques de votre réseau local.

-	**La notion de dialplan**

Le plan de numérotation Freeswitch est un mécanisme complet, basé sur XML. Il est recommandé que vous compiliez Freeswitch avec la configuration par défaut et assurez-vous qu'il fonctionne avant de commencer à faire des personnalisations. Notez que les fichiers de configuration par défaut, y compris le plan de numérotation de default.xml, sont souvent mis à jour.

-	**Présentations du module python**

Python est un langage de programmation orienté objet puissant pour écrire des programmes. le module python il permet d'interagir avec FreeSwitch à l'aide de scripts écrits en langage python.


-	**Notions de Gateway**

Gateway est un système qui permet de coupler freeswitch avec un autre système de téléphonie. Pour que les utilisateurs de freeswitch puissent appeler et recevoir des appels de l'extérieur (1 autre ipbx) il faut faire appel aux notions de passerelles ( Gateway ).


![Notion de Gateway freeswitch](./images/free2.png)


-	**Dialplan avancé et applications externes**

Dialplans sont utilisés pour acheminer un appel destiné à son critère d'évaluation appropriée, qui peut être une extension, la messagerie vocale, de réponse vocale interactive (IVR) menu traditionnel ou une autre application compatible. Dialplans sont extrêmement flexibles.

Dialplans peuvent être séparés en «contextes» nous permettant d'avoir Dialplans séparés pour différents types d'appels. Les appels peuvent être remis hors de différents contextes. Par exemple, pour soulager les problèmes de sécurité, vous pouvez avoir deux dialplans, qui gère les appels en provenance du réseau téléphonique public (PSTN) et qui gère les appels provenant de postes internes. Les fichiers de configuration de l'échantillon FreeSwitch utilisent cette méthode, forçant un appel PSTN entrant à travers un certain contrôle supplémentaire avant d'être transféré au dialplan interne. Une autre utilisation d'un contexte de dialplan vous permet de partager un seul PBX avec plusieurs locataires dans un immeuble de bureaux. Étant donné que chaque locataire aura probablement leur propre ensemble de (et souvent contradictoires) des extensions, des menus vocaux, etc., il est logique de séparer les locataires dans leurs propres dialplans indépendants pour faciliter la configuration et la maintenance.



### 2. Installation de Freeswitch

#### **Pré-requis**

	sudo apt -y update
	
	sudo apt install -y git subversion build-essential autoconf automake libtool libncurses5 libncurses5-dev make libjpeg-dev libtool libtool-bin libsqlite3-dev libpcre3-dev libspeexdsp-dev libldns-dev libedit-dev yasm liblua5.2-dev libopus-dev cmake
	
	sudo apt install -y libcurl4-openssl-dev libexpat1-dev libgnutls28-dev libtiff5-dev libx11-dev unixodbc-dev libssl-dev python-dev zlib1g-dev libasound2-dev libogg-dev libvorbis-dev libperl-dev libgdbm-dev libdb-dev uuid-dev libsndfile1-dev
	
	sudo apt install -y cmake
	cd /usr/src
	sudo git clone https://github.com/signalwire/libks.git
	cd libks
	sudo cmake .
	sudo make
	sudo make install
	
	cd /usr/src
	git clone https://github.com/signalwire/signalwire-c.git
	cd signalwire-c
	sudo cmake .
	sudo make
	sudo make install
	
	cd /usr/src
	sudo wget https://files.freeswitch.org/freeswitch-releases/freeswitch-1.10.3.-release.zip
	sudo apt -y install unzip
	sudo unzip freeswitch-1.10.3.-release.zip
	cd freeswitch-1.10.3.-release/


	sudo apt -y install unzip

	sudo ./configure -C

![Compilation freeswitch](./images/free3.png)


Si vous avez des erreurs lors de la compilation, vous allez commenter les lignes suivantes dans le fichier **modules.conf** du dossier source de freeswitch :

	languages/mod_lua 
	applications/mod_signalwire

**NB:** Vous pouvez aussi décommenter l'ensemble des modules qui vous interressent dans le même fichier **modules.conf** comme le module **python, sms, etc.**. Mais aussi chaque fois qu'un module cause problème alors qu'il n'est pas nécessaire, vous pouvez commenter la ligne le concernant dans le fichier.

Ensuite refaire:
	
	sudo ./configure -C

	sudo make


Si la compilation s'est bien passée, vous devez avoir une image comme ceci:

![Commande make de freeswitch](./images/free4.png)


Terminez par la commande suivante:

	sudo make install


Et vous aurez:

![Fin installation de freeswitch](./images/free5.png)



### 3. Configuration de Freeswitch

Après installation de Freeswitch,le dossier le configuration est placé dans **/usr/local/freeswitch** 


![Contenu du dossier freeswitch](./images/free6.png)


Le dossier **conf** contient un ensemble de dossiers et de fichiers de configuration pour freeswitch


![Contenu du dossier conf](./images/free7.png)


Le dossier **autoload_configs** contient un ensemble de fichiers de configuration des différents modules disponibles 


![Contenu du dossier autoload_configs](./images/free8.png)


Le dossier **dialplan** contient les dossiers et fichiers de configuration des plans de numérotation


![Contenu du dossier dialplan](./images/free9.png)

Ce dossier dialplan contient un fichier **default.xml** contenant les configurations par défaut du plan de numérotation

Le dossier **directory** contient un dossier **default** contenant les comptes des utilmisateurs par défaut de freeswitch


![Contenu du dossier dialplan](./images/free10.png)

![Contenu du dossier default](./images/free11.png)


Nous allons utiliser ces comptes pour les tests que nous allons faire.


### 4. Test de fonctionnement 


Nous allons d'abord démarrer freeswitch, pour cela nous allons créer n lien symbolique vers les scripts de démarrage comme suit:

	ln -s /usr/local/freeswitch/bin/freeswitch /usr/bin/freeswitch
	ln -s /usr/local/freeswitch/bin/fs_cli /usr/bin/fs_cli


Pour démarrer le freeswitch, on utilise:

	freeswitch -nc   // pour démarrer en arrière plan (background)
	

Il faut attendre quelques instant après, pour faire:

	fs_cli


On doit alors voir la console de déboguage de freeswitch comme suit:

![Console de freeswitch](./images/free12.png)


Après chaque modification dans les fichiers de configuration de freeswitch, nous devons recharger avec :

	reloadxml



-	**Test d'appel**


Nous allons connecter deux comptes **1000** et **1001** pour tester


**NB:** le mot de passe par défaut des comptes est **1234**.



**Compte 1000**

![Compte 1000](./images/free13.jpeg)


**Compte 1001**

![Compte 1001](./images/free14.jpeg)


**Console freeswitch**

![Compte 1001](./images/free15.png)


Pour quitter la console on fait: **/exit** ou **/quit**



### 5. Interconnection entre Freeswitch et Asterisk


**Gestion des appels entrants d'asterisk vers FreeSWITCH**

#### 5.1 Configuration côté Freeswitch

On définit d'abord le plan de numérotation des appels entrants dans le fichier **/usr/local/freeswitch/conf/dialplan/public/00_inbound_did.xml** comme montre l'image ci-dessous


![Compte pour les appels entrants](./images/free16.png)


#### 5.2 Configuration côté Astérisk


On définit le compte trunk vers freeswitch comme suit:


	;Compte trunk vers freeswitch
	[1019]
	type=registration
	transport=transport-udp
	outbound_auth=1019
	server_uri=sip:192.168.2.119    ;adresse IP du freeswitch
	client_uri=sip:1019@192.168.2.119
	retry_interval=60
	expiration=120
	contact_user=1019
	line=yes
	endpoint=1019
	

	[1019]
	type=endpoint
	context=ec2lt-toip
	disallow=all
	allow=h263p
	allow=h264
	allow=ulaw
	allow=vp8
	outbound_auth=1019
	aors=1019
	from_user=1019
	from_domain=192.168.2.119
	direct_media=no


	[1019]
	type=auth
	auth_type=userpass
	password=1234
	username=1019


	[1019]
	type=aor
	max_contacts=30
	contact=sip:192.168.2.119
	qualify_frequency=60


	[1019]
	type=identify
	endpoint=1019
	match=192.168.2.119

	;Fin trunk freeswitch


Ensuite on va dans /etc/asterisk/extensions.conf de asterisk pour définir le numéro vers laquelle on peut joindre les utilisateurs de FreeSWITCH avec la ligne suivante.

	exten => _01XXX,1,Dial(PJSIP/${EXTEN:1}@1019) ;On compose par exemple 01000 pour appeler le compte 1000 de freeswitch en enlevant le 0 par l'option :1 de EXTEN


Cette extension veut dire tout numéro commençant par **01 suivi de 3** autres chiffres envoie l'appel vers le compte **1019**. Elle doit être définie dans le contexte de vos utilisateurs


#### 5.3 Test de fonctionnement

**Compte 7000 d'astérisk**

![Compte 7000 d'asterisk](./images/free17.jpeg)


**Compte 1000 de freeswitch**

![Compte 1000 freeswitch](./images/free18.jpeg)



**Console de Freeswitch**


![Console freeswitch](./images/free19.png)



**Console d'Astérisk**


![Consolde d'asterisk](./images/free20.png)



**Gestion des appels sortants de FreeSWITCH vers asterisk**

#### 5.1 Configuration côté Freeswitch

Tous d'abord on dans le dossier **/usr/local/freeswitch/conf/sip_profiles/external/** on crée un fichier **asterisk.xml**
Dans ce fichier on met les instructions suivantes en chageant l'adresse par votre adresse IP

	<include>
	   <gateway name="asterisk">
	        <param name="username" value="1019"/>
	        <param name="password" value="1234"/>
	        <param name="realm" value="192.168.2.110"/>
	        <param name="proxy" value="192.168.2.110"/><!-- ASTERISK ADDRESS -->
	        <param name="register" value="true"/>
	   </gateway>
	</include>


Puis sur l’interface de FreeSWITCH, on recharge le module sofia qui gere sip pour que FreeSWITCH se connecte à asterisk par la commande **reload mod_sofia**
Automatiquement on voie sur la console de asterisk que freeswitch s’enregistre sur asterisk



Maintenant on définit un plan de numérotation sur freeswitch pour dire comment acheminer les appels vers asterisk dans le fichier **/usr/local/freeswitch/conf/dialplan/default.xml** juste avant l'extension **Local_Extension_Skinny** comme:


	<extension name="ast_extens">
	        <condition field="destination_number" expression="^(7\d{3})$">
	          <action application="bridge" data="sofia/gateway/asterisk/$1"/>
	        </condition>
	</extension>


![plan de numérotaio sortant vers asterisk](./images/free22.png)


IL faut redémarrer Freeswitch comme suit:

	fs_cli
	shutdown


Ensuit faire:

	freeswitch -nc

Attendre quelques minutes et fair:

	fs_cli
	reloadxml
	reload mod_sofia


On peut voir dans l'interface d'Astérisk:

![Gateway connecté sur asterisk](./images/gateway.png)


#### 5.3 Test de fonctionnement



Pour tester, l'utilisateur **1000** de freeswitch appel l'utilisateur **7000** d'astersik.


**7000 d'astérisk**

![Utilisateur 7000  d'asterisk](./images/free23.jpeg)



**1000 de freeswitch**

![Utilisateur 1000  de freeswitch](./images/free24.jpeg)



**console de freeswitch**

![Console de freeswitch](./images/free-consol.png)



**console d'asterisk**

![Console d'asterisk](./images/free-aster.png)



## Etape 9: Kamailio SRV/NAPTR (free.sn et orange.sn)

### 1. Définition de NAPTR

Un pointeur d'autorité de nom ( NAPTR ) est un type d' enregistrement de ressource dans le système de noms de domaine d'Internet.
Les enregistrements NAPTR sont le plus souvent utilisés pour des applications de téléphonie. Internet, par exemple, dans le mappage de serveurs et d'adresses d'utilisateur dans le protocole SIP ( Session Initiation Protocol ). La combinaison d'enregistrements NAPTR avec des enregistrements de service (SRV) permet l'enchaînement de plusieurs enregistrements pour former des règles de réécriture complexes qui produisent de nouvelles étiquettes de domaine ou des identificateurs de ressources uniformes (URI).
Les noms de ressources uniformes ( URN ) sont un sous-ensemble d'identificateurs de ressources uniformes ( URI ) utilisés pour les identificateurs abstraits, tels que le nomd'une personne ou son numéro de téléphone. Pour que les URN aient un sens, elles doivent être mappées sur une ressource concrète quelconque. Les URL (Uniform Resource Locator) sont souvent utilisés pour décrire de telles ressources, telles qu'un nom d'hôte d' ordinateur ou un fichier local.
L’enregistrement NAPTR facilite la normalisation des URN. Les enregistrements NAPTR mappent entre des ensembles d'URN, d'URL et de noms de domaine simples et suggèrent aux clients les protocoles disponibles pour la communication avec la ressource mappée.

### 2. Définition de SRV

Un enregistrement SRV ou enregistrement de service est une catégorie de données du DNS d'Internet qui vise à indiquer quels sont les services disponibles.
Ce type d'enregistrement est défini dans la RFC 2782.Les enregistrements SRV sont souvent utilisés par les clients Microsoft Windows afin de trouver le contrôleur de domaine pour un service donné.
Par ailleurs, les enregistrements SRV sont communément utilisés en association avec les protocoles standards ci- dessous :
-	XMPP (Jabber)
-	SIP
-	LDAP


### 3. Objectifs

Notre objectif est d’utiliser les enregistrements de type SRV et NAPTR en téléphonie sur IP pour :
-	Faciliter le paramétrage des terminaux SIP;
-	Permettre aux terminaux, connaissant simplement le nom de domaine, de faire des requêtes SRV pour avoir l’adresse IP du Serveur et pouvoir se connecter ;
-	Permettre l’interconnexion de deux serveurs de téléphonie ;
-	Permettre l’utilisation du TEL/URI au lieu du SIP/URI pour pouvoir joindre les abonnes se trouvant dans d’autres serveurs SIP

Le but de cette partie est de faire intervenir un DNS dans les configurations de Kamailio afin de pouvoir joindre les abonnés par TEL/URI


### 4. Mise en place du DNS

	apt install bind9 bind9utils
	cd /etc/bind
	vim named.conf.default-zones

	zone "ec2lt.sn" {
		type master;
		file"/etc/bind/ec2lt.zone";
	};


	cp db.local ec2lt.zone
	vim ec2lt.zone


On ajoute le contenu suivant:

	;
	; BIND data file for local loopback interface
	;
	$TTL    604800
	@       IN      SOA     dns.ec2lt.sn. latyr.ec2lt.sn. (
	                              2         ; Serial
	                         604800         ; Refresh
	                          86400         ; Retry
	                        2419200         ; Expire
	                         604800 )       ; Negative Cache TTL
	;
	ec2lt.sn.       IN      NS      dns.ec2lt.sn.
	dns.ec2lt.sn.       IN      A       192.168.2.123


Ensuite faire:
	
	service bind9 restart

On édite le fichier **/etc/resolv.conf** comme suit:

	nameserver 192.168.2.123   ;l'adresse de notre machine locale


On peut vérifier si tout fonctionne:

	nslookup
	> set type=any
	> ec2lt.sn

		Server:		192.168.2.123
		Address:	192.168.2.123#53

		ec2lt.sn
			origin = dns.ec2lt.sn
			mail addr = latyr.ec2lt.sn
			serial = 2
			refresh = 604800
			retry = 86400
			expire = 2419200
			minimum = 604800
		ec2lt.sn	nameserver = dns.ec2lt.sn.


![Test de fonctionnement](./images/kamsrv0.png)


Si on fait **ping** sur le domaine on voit bien:

	ping ec2lt.sn

![Ping sur ec2lt.sn](./images/kamsrv1.png)


### 5. Les enregistrements SRV

Un enregistrement SRV ou enregistrement de service est une catégorie de données du DNS d'Internet qui vise à indiquer quels sont les services disponibles.
Ce type d’enregistrement est souvent utilisé en association avec certains protocoles standards comme : XMPP, SIP, LDAP.

**La syntaxe d'un enregistrement de type SRV** 

-	**Service** : le nom symbolique (commençant généralement par un symbole de soulignement) du service concerné (par exemple \_sip ).
-	**Protocole** : généralement, c'est soit _tcp pour TCP, soit _udp pour UDP.
-	**Nom.de.domaine** : le domaine de validité de l'enregistrement (pleinement qualifié au format FQDN ou local à la zone DNS en cours de définition pour la même autorité d'origine).
-	**TTL** : champ standard DNS indiquant la durée de validité (Time-To-Live, durée de vie) de la réponse (en secondes).
-	**Classe** : champ standard DNS indiquant la classe d'adressage (c'est toujours IN pour Internet).
-	**Type** : l'identifiant du type d'enregistrement DNS (toujours SRV ici pour un enregistrement de service) ;
-	**Priorité** : la priorité du serveur cible (valeur entière non négative, plus elle est faible, plus ce serveur sera utilisé s'il est disponible) ;
-	**Poids** : poids relatif pour les enregistrements de même priorité ;
-	**Port** : le numéro de port (TCP ou UDP selon le protocole ci-dessus) où le service est disponible ;
-	**Cible** : le nom du serveur qui fournit le service concerné (doit être résolu en adresse IPv4 ou IPv6 par d'autres requêtes DNS sur les enregistrements A ou AAAA du nom de service cible) avec le protocole et sur le port indiqué.


**Mise en oeuvre** 


-	**SRV**

On édite le fichier:

	cd /etc/bind9
	vim ec2lt.zone

![Mise en oeuvre srv](./images/kamsrv2.png)


Redémarrer le serveur
	
	service bind restart


Tester les enregistrements SRV comme suit :

Pour le canal udp

	dig _sip._udp.ec2lt.sn SRV


![Test SRV udp](./images/kamsrv3.png)


Pour le canal tcp

	dig _sip._tcp.ec2lt.sn SRV


![Test SRV tcp](./images/kamsrv4.png)


-	**NAPTR**

On édite le fichier:

        cd /etc/bind9
        vim ec2lt.zone

![Mise en oeuvre naptr](./images/kamsrv5.png)


**NB:** Les numéros Kamailio sont sous la forme 5XX


Redémarrer le serveur

        service bind restart


On peut aussi tester les enregistrement NAPTR comme suit :

	dig -t naptr 0.0.5.ec2lt.sn


![Test fonctionnement enregistrement naptr](./images/kamsrv6.png)


On voit bien que l'enrégistrement naptr fonctionne bien


### 6. Chargement du module enum.so

ENUM, est un mécanisme permettant d'utiliser un numéro de téléphone comme clé de recherche dans le DNS pour trouver la manière de joindre une personne ou une autre entité.
L'objectif d'ENUM est d'utiliser un numéro unique pour accéder à plusieurs identifiants de service caractérisant un individu : Grâce au numéro ENUM, on pourra atteindre un e-mail, une URL, un numéro de téléphone (fax, mobile, SIP), etc. 

Les priorités d'accès à ces différents services seront définies par l'utilisateur.

	cd /etc/kamailio
	vim kamailio.cfg

	mpath="/usr/lib/x86_64-linux-gnu/kamailio/modules/"


Activer le module enum

	loadmodule "enum.so"


Création de la route enum

	#route pour enum
	route(ENUM);


Définition de la méthode


	route[ENUM] {

		if(uri==myself){
			if($rU=~"^\+5")
				if((enum_query("ec2lt.sn. ")))
					route(RELAY);
		}
	}


Préciser au niveau du paramètre alias le domaine qu'il gère

	/* add local domain aliases */
	alias="ec2lt.sn"


Redémarrer le serveur

	service kamailio stop
	service kamailio start


### 6. Test de fonctionnement

Configuration du compte **501** sur **MicroSip**

Dans **Paramètres** de MicroSip:

![Configuration dans parametres](./images/confparam.PNG)


Paramètrages du comptes

![Parametrage du compte](./images/confcompte.PNG)

Connexion du compte

![Connexion du compte](./images/connect.PNG)


**NB:** Nous ferons les mêmes configurations pour le compte **500** avec MicroSip pour tester les appels.


**Test d'appels**

**500 appel 501**

![Compte 500 appel 501](./images/entree1.PNG)


![Compte 500 appel 501 décroche](./images/entree2.PNG)



**501 appel 500**

![Compte 501 appel 500](./images/entre1.PNG)


![Compte 501 appel 500 décroche](./images/entre2.PNG)



## Etape 10: Interco entre asterisk pjsip et l'opérateur ippi


### 1. Configuration côté Opérateur IPPI


-	D'abort il faut créer un compte chez l'opérateur ippi à l'adresse suit: [créer un compte ippi](https://www.ippi.com/?lang=fr)

-	Ensuite vous pouvez accéder à votre **Dashboard** comme suit:

![Dashboard ippi](./images/ippi1.png)



-	Sur le button **PARAMÈTRES NUMÉROS**, vous trouverez les informations pour la paramétrage du compte SIP comme suit:


![Parametres numeros](./images/ippi2.png)



-	Pour le **numéro** et **l'identifiant**, vous trouverez dans le Dashboard comme suit:


![Identifiant et numero du compte](./images/ippi3.png)



### 2. Configuration côté Astérisk

-	**Dans pjsip.conf**

Pour la configuration côté Astérisk, nous allons créer un compte trunk pour l'opérateur IPPI dans le fichier **pjsip.conf** comme suit:


	;Compte trunk vers Opérateur ippi

	[ippi]
	type=registration
	transport=transport-udp
	outbound_auth=ippi
	server_uri=sip:sip.ippi.com   ;domaine du compte ippi
	client_uri=sip:Latyrndiaye16_@sip.ippi.com
	retry_interval=60
	expiration=120
	contact_user=Latyrndiaye16_
	line=yes
	endpoint=ippi


	[ippi]
	type=endpoint
	context=ec2lt-toip
	disallow=all
	allow=h264
	allow=ulaw
	allow=vp8
	outbound_auth=ippi
	aors=ippi
	from_user=Latyrndiaye16_
	from_domain=sip.ippi.com
	direct_media=no
	

	[ippi]
	type=auth
	auth_type=userpass
	password=Passer123/
	username=Latyrndiaye16_
	

	[ippi]
	type=aor
	max_contacts=30
	contact=sip:sip.ippi.com
	qualify_frequency=60


	[ippi]
	type=identify
	endpoint=ippi
	match=sip.ippi.com

	;Fin trunk ippi


-	**Dans extensions.conf**

Dans le fichier extensions.confn nous allons définir un plan de numérotation pour envoyer les appels vers l'opérateur IPPI comme suit:

	exten => 889758196,1,Dial(PJSIP/${EXTEN}@ippi)


On recharge asterisk avec:

	asterisk -rvvvvvvvvvvvvv
	reload

On peut voir si le compte ippi s'est enrégistré sur le serveur asterisk

	pjsip show registrations

![Enregistrement du compte ippi](./images/ippi4.png)



### 3. Test de Fonctionnement

-	**On connecte d'abord le compte ippi sur un softphone *zoiper* par exemple comme suit:**


![Configuration du compte ippi sur zoiper](./images/ippicompte1.png)


-	**Connexion du compte ippi avec zoiper**


![Connexion du coppte ippi sur zoiper](./images/ippicompte2.png)


-	**On connecte un autre client softphone sur le serveur astérisk.**




**Test des appels**

-	**De astérisk vers opérateur ippi**


**Utilisateur 7000 asterisk**


![compte 7000 asterisk](./images/ippicompte3.jpeg)



**Utilisateur Latyrndiaye16\_ de ippi**

![compte Latyrndiaye16\_ de ippi](./images/ippicompte4.jpeg)



**Console asterisk**

![console asterisk](./images/ippi5.png)




